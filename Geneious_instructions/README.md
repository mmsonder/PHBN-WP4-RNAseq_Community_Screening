## MAPPING IN GENEIOUS
-------------------------

### 1. Importing read files and SILVA database

#### 1.1 Create new foler
Right click on the existing folders (or the one called “Local”) to create a new folder.

![](images/Create_NewFolder.PNG)

#### 1.2 Import read files

Once you have clicked on the new folder, you can import read files (mostly .fastq) and the SILVA database (in fasta format) in this folder via **menu File -> Import -> From file…**
The custom SILVA database should be downloaded from [here](https://gitlab.com/ahaegeman/PHBN-WP4-RNAseq_Community_Screening/blob/master/SILVA_132_LSU_database/SILVA_132_LSURef_tax_silva_trunc_renamed.fasta).

![](images/Import_ReadFiles.PNG)

_**Note:**_ *You can also drag and drop the read files into the Geneious window.*


If you work with paired reads, you can choose to import them separately or to pair them.
You can also pair the reads after the importation, via **menu Sequence -> Set Paired Reads**.


### 2. Read Mapping

**Mark the imported read file(s) to which you want to map.**
You can map reads against the SILVA database via **menu Align/Assemble -> Map to Reference**.

![](images/MapToReference.PNG)

Once The 'Map to Reference window' is opened, you need to select the database against which reads will be mapped by clicking the 'Choose' button. *In this case it is the SILVA database in fasta format*.


In order to be able to modify the percentage of sequence identity, the Sensitivity should be set on “Custom Sensitivity”.

![](images/ImportSilvaDatabase.PNG)


You have to write **a name for your mapping**, for example : *'Mapping reads against SILVA database'*

Make sure that the following options are marked:

✔ Save assembly report  <br/>
✔ Save list of used reads  <br/>
✔ Save in sub-folder  <br/>
✔ Save contigs  <br/>

![](images/AssemblyName.PNG)

In the Advanced section, the following option allows to modify the percentage of identity.
* The “Maximum Mismatches Per Reads”, which is defined as the maximum number of single base mismatches allowed per read as a percentage of the read length. In the example, it is set at 15%.

![](images/AdvancedSettings.PNG)

→ You can launch the mapping by clicking on **OK**.  <br/>  A warning message will appear because there are a lot of reference sequences in the database, but if you click on **“Continue”** it should still work.

![](images/WarningMessage.PNG)


Once the mapping is done, a new folder called according to the assembly name is created.
The folder contains an assembly report as well as every reference with reads mapped on them. The “Name” column shows the reference sequence name and the “Sequence” column shows the number of reads mapped on the reference.

![](images/AssemblyResults.PNG)

----*Optional :* Sometimes one of the columns can be absent from the table. To add a column, do a right click on the table header and tick the absent column.----

![](images/OptionColomnInformation.PNG)

### 3. Export Mapping Results
To export the results, click on the box in the upper-left corner. *All documents should be highlighted in blue*.
The table can be exported via menu **File -> Export -> Selected Documents…**

![](images/ExportSelectedDocuments.PNG)


The file needs to be exported in **TSV tab-separated table (.tsv)** <br/>
Click on **proceed**. <br/>
You can now choose the columns that will be exported in a tsv-separated table.

![](images/TSVExport.PNG)

![](images/TSVExportFields.PNG)

Make sure that the following fields are exported:

✔ Name <br/>
✔ # Sequences <br/>
✔ Pairwise identity <br/>
✔ Ref Seq Length  <br/>

Click on **OK**. <br/>
You can find your exported TSV file in the choosen export location.

You can **upload this file** to the *Results_files subfolder in the [WP4 - RNAseq community effort](https://drive.google.com/drive/folders/1Wyp-jJV_7m9TKKnWOjnYevfWR1S1Agma?usp=sharing) Google drive PHBN folder.* Don't forget to also complete the **metadata** [template](https://drive.google.com/open?id=1uFWrBK5xfABpAMEdoLGJirl60Qw2naTaN_WHPDzApSI) with some extra info on the samples you analyzed.

In case you want to explore the results yourself, you can open the TSV formatted file in for example Notepad++ en copy it to Excel to view the results in separated columns. If you export more fields in the TSV export file, you will have more information on the mapped reads for further analysis.
