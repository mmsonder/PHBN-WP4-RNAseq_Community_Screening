# PHBN-WP4-RNAseq_Community_Screening

## Introduction
The goal of this “community effort” is to **screen existing total RNA-seq datasets** for the presence of (r)RNA to have an idea about the relative quantity and the composition of this (r)RNA (plants, fungi, oomycetes, bacteria, insects, phytoplasms). This will yield information on whether or not the datasets used for virus detection also have value in **detecting other non-viral plant pathogens**.

Each partner will do a **first round of screening their own datasets**. This can be done in a relative simple way by mapping all (cleaned) reads to a provided rRNA database (SILVA LSU). This first round of screening will give a **rough idea on the taxonomical composition of the rRNA fraction** of the data. We will count the number of reads which mapped to the rRNA of different organisms, and we will summarize these counts by taxonomically relevant groups and communicate this with the researcher.

**In a second round, the researcher can select a few datasets which show promising results (i.e. lots of non-plant rRNA)**. ILVO will then do a more detailed metagenomics analysis using for example [Kraken2](https://ccb.jhu.edu/software/kraken2/) and/or meta-assemblies and mapping, all in consultation with the researcher.

## Examples

Examples of what to expect are shown [here](https://gitlab.com/ahaegeman/PHBN-WP4-RNAseq_Community_Screening/tree/master/Examples).

## Quick start instructions for the impatient
1) **Map reads** of your samples of choice against the [provided database](https://gitlab.com/ahaegeman/PHBN-WP4-RNAseq_Community_Screening/blob/master/SILVA_132_LSU_database/SILVA_132_LSURef_tax_silva_trunc_renamed.fasta) (renamed version of SILVA LSU v132)
- Use total RNA-seq data preferrably with adapters and low quality reads removed
- Use a mapping algorithm of your choice, but allow random mapping in case of two or more matches with the same score; and do not put the %identity threshold too strict (e.g. 85%)


2) Copy the **metadata** template which can be found in the [Google Drive Folder](https://drive.google.com/drive/folders/1Wyp-jJV_7m9TKKnWOjnYevfWR1S1Agma?usp=sharing) to the ["Metadata_files" folder](https://drive.google.com/open?id=1uFWrBK5xfABpAMEdoLGJirl60Qw2naTaN_WHPDzApSI) and rename it with your institute and name (for example: "ILVO_Annelies_Haegeman.xlsx"). Complete the metadata file with some metadata on your samples, 1 sample per column.


3) **Upload 1 result file per sample** to the ["Result_files" folder](https://drive.google.com/open?id=1p_I2fY5kzFpLFX09-gMhT5sdk5ge3CFj) on Google Drive containing the number of reads mapped to each sequence of the database.
- use the sample name (as specified in the metadata file) as file name and include your institute and your initials (for example "ILVO_AH_Sample1.txt")
- preferrably a tab delimited text (or Excel) file containing at least the database sequence ID and the number of reads which mapped against each sequence
- Example:
```
AY224383.3948.6873,Bacteria,Bacteria;Firmicutes;Bacilli;Bacillales;Bacillaceae;Bacillus;Bacillus_cereus    238
AY244366.1.2800,Bacteria,Bacteria;Proteobacteria;Alphaproteobacteria;Rhizobiales;Rhizobiaceae;Mesorhizobium;Mesorhizobium_huakuii       120
AY244374.1.2802,Bacteria,Bacteria;Proteobacteria;Alphaproteobacteria;Rhizobiales;Rhizobiaceae;Allorhizobium-Rhizobium;Allorhizobium_vitis       0
CBTL0109833347.1.1973,Organelles,Eukaryota;Archaeplastida;Chloroplastida;Charophyta;Phragmoplastophyta;Triticum_aestivum_(bread_wheat)  607
GFKT013550593.493.3960,SpidersAndMites,Eukaryota;Opisthokonta;Holozoa;Metazoa_(Animalia);Eumetazoa;Bilateria;Arthropoda;Chelicerata;Arachnida;Nephila_clavipes    50
```


## In detail instructions for data analysis and reporting
### Suitable data
Illumina **total RNA-seq** data (with or without rRNA depletion step) derived from plant material, sequenced with **at least 1x75 bp**, ideally 2x150 bp or 2x300 bp is suited for this project, the longer the reads, the more reliable the mappings.

You can analyze as many of your samples as you want.
Samples from which you suspect a (co-)infection with a non-viral plant pathogen are especially welcomed.

### Preprocessing of data

In most cases, the (preprocessed) datasets you used for other analyses should be suited to directly map to the database.

Some minimal preprocessing of your reads is recommended:
- Remove remaining adapters
- Filter low quality reads

Optional preprocessing methods:
- Paired end merging of the reads
- Removal of duplicate reads
- Plant host removal


### Mapping of preprocessed data

#### Introduction
IMPORTANT REMARK: **in case you did a rRNA removal step during your data analysis** (using for example one of the following programs: SortMeRNA, rRNAFilter, phyloFlash), make sure to either use the rRNA fraction for further analysis or use the data before you did the rRNA removal step.

Mapping should be done against the **provided database of LSU rRNA**. This dataset is derived from the latest SILVA (https://www.arb-silva.de/) files (v132, fasta files), where we have changed the headers of each sequence to also contain the organism group it belongs to. The fasta headers are in this format: `>ID,group,SILVA_taxonomy`.
The “group” is one of the following: *Organelles, Phytoplasms, Bacteria, Fungi, Plants, Nematodes, Oomycetes, Insects, SpidersAndMites and Others*.
A few examples of headers of sequences are shown here:
```
>JQ766308.1.1248,Phytoplasms,Bacteria;Tenericutes;Mollicutes;Mollicutes_Incertae_Sedis;Unknown_Family;Candidatus_Phytoplasma;Cabbage_stunt_phytoplasma
>GY203941.1.1493,Bacteria,Bacteria;Bacteroidetes;Bacteroidia;Bacteroidales;Prevotellaceae;Prevotella_7;unidentified
>MKYQ01000643.5377951.5379539,Organelles,Bacteria;Cyanobacteria;Oxyphotobacteria;Chloroplast;Citrus_maxima
```

**Mapping** can be done using any software you prefer, as long as you:
- do not put the identity thresholds too strict (e.g. 85% is OK)
- allow random mapping in case of two matches with the same score

In case you need help for the mapping, some instructions are given below.

As result, we do NOT need bam/sam files, but rather need a **statistics file** which says (at least) **how many reads mapped against each sequence**. If you are not sure how to get this statistics file, please check the instructions below. If you are still puzzled after reading the instructions, you can [contact](mailto:annelies.haegeman@ilvo.vlaanderen.be) us for help.

#### Instructions on how to map using the software Geneious
Check [this link](https://gitlab.com/ahaegeman/PHBN-WP4-RNAseq_Community_Screening/tree/master/Geneious_instructions)

#### Instructions on how to map using the software CLC Genomics Workbench
Check [this link](https://gitlab.com/ahaegeman/PHBN-WP4-RNAseq_Community_Screening/tree/master/CLC_instructions)

#### Instructions on how to map using command line tools (BWA)
Check [this link](https://gitlab.com/ahaegeman/PHBN-WP4-RNAseq_Community_Screening/tree/master/BWA_instructions).

### Report your results

The reporting is done by doing two things explained below:
1) uploading the result of your mapping, 1 file per sample
2) completing a spreadsheet with some metadata, 1 column per sample

#### Result of the mapping
As reporting format, we need **1 file per sample** which shows the number of mapped reads per sequence. The file needs to contain at least the **sequence ID and the number of reads which mapped on the sequence**, ideally in tab delimited or csv delimited format. If you use graphical software such as CLC Genomics Workbench or Geneious, this information can be exported from the software as explained in the links provided above. If you use command line mapping tools, you can simply use `samtools idxstats` to generate a file which contains the number of mapped reads against each sequence. For more information on how to do this, check the detailed instructions mentioned above.

The result file should **include the sample name as file name** to be able to link your results file to your metadata table (see below). It should also contain your institute and your initials, **for example "ILVO_AH_Sample1.txt"**.

The result files of your different samples should be uploaded in the ["Result_files" folder](https://drive.google.com/open?id=1p_I2fY5kzFpLFX09-gMhT5sdk5ge3CFj) on Google Drive.

#### Metadata of the analyzed samples


Copy the **metadata** template which can be found in the [Google Drive Folder](https://drive.google.com/drive/folders/1Wyp-jJV_7m9TKKnWOjnYevfWR1S1Agma?usp=sharing) (in Excel or in Google docs format) to the ["Metadata_files" folder](https://drive.google.com/open?id=1uFWrBK5xfABpAMEdoLGJirl60Qw2naTaN_WHPDzApSI) on Google Drive and rename it with your name and institute, **for example: "ILVO_Annelies_Haegeman.xlsx"**. Complete the metadata file with the requested metadata on your samples, 1 sample per column. Please make sure to mention the correct sample name in the metadata file to be able to link it to the mapping results file (for example "ILVO_AH_Sample1.txt").

## Timing
| Period              	| What                                  	| Deadline                         	|
|---------------------	|---------------------------------------	|----------------------------------	|
| Dec 2019 - April 2020 	    | Participants analyze datasets         	| April 30: submit results         	|
| Dec 2019 - Sep 2020	| ILVO does detailed analysis on selected samples  	| Sep 30: report to participants, general report 	|
| Autumn 2020  	| Discussion among partners on future plans 	| Dec 31: plan for 2021  	|



## Acknowledgements

This research is partially funded by the Belgian Federal Public Service of Health, Food Chain Safety and Environment ([FPS Health](https://www.health.belgium.be/en)) through the contract "RI 18_A-289" and by the [Euphresco](www.euphresco.net) project "Plant Health Bioinformatics Network" (PHBN) (2018-A-289).
